jQuery(document).ready(function ($) {
  window.twttr = (function (d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0],
      t = window.twttr || {};
    if (d.getElementById(id)) return t;
    js = d.createElement(s);
    js.id = id;
    js.src = "https://platform.twitter.com/widgets.js";
    fjs.parentNode.insertBefore(js, fjs);

    t._e = [];
    t.ready = function (f) {
      t._e.push(f);
    };

    return t;
  })(document, "script", "twitter-wjs");
  // -- General -- //
  const $GeneralScope = {
    // Constructor
    init: function () {
      this.menuScripts();
      this.navigationSnippet();
      this.moveSectionFooterIntoFooter();
      this.setBackgroundImage();
      this.HideClickMenu();
      this.sliderVideo();
      this.twiterWiget();
    },

    twiterWiget: function () {
      TweetJs.ListTweetsOnUserTimeline("@NileSpecial", function (data) {
        console.log(data, "data");

        var json = data;
        var counter = 0;
        var baseWidth = 300;
        $(function () {
          var elem = document.querySelector("#container2");

          twttr.ready(function (twttr) {
            rayoutTimeline().then(function () {
              jQuery("#container2").masonry({
                itemSelector: ".grid-item",
                columnWidth: ".grid-sizer",
                gutter: 5,
              });
            });
          });
        });

        function rayoutTimeline() {
          var d = new $.Deferred();
          var $timeline = $("#timeline");

          $.each(json, function (i, item) {
            if (i < 2) {
              var $grid = $('<div class="grid-item">');
              $timeline.append($grid);
              $grid.attr("data-index", i);
              twttr.widgets
                .createTweet(item.id_str, $grid.get(0), {
                  align: "left",
                  width: baseWidth,
                  //cards: 'hidden',
                  conversation: "none",
                })
                .then(function (el) {
                  var $el = $(el);
                  var $pl = $el.parent();
                  var index = parseInt($pl.attr("data-index"));
                  $el.css({ opacity: 1, "transition-delay": index / 10 + "s" });
                  counter++;
                  if (counter === json.length) {
                    console.log(counter);
                    return d.resolve();
                  }
                });
            }
          });

          return d.promise();
        }
      });
    },

    // Menu scripts
    menuScripts: function () {
      let ButtonTrigger = $(".navbar-header .navbar-toggle");
      let MenuWrapper = $(".navbar-header .navbar-collapse");

      if (ButtonTrigger) {
        $(document).on("click", ".navbar-toggle", function () {
          ButtonTrigger.toggleClass("collapsed");
        });
      }

      $(window).scroll(function () {
        let scroll = $(window).scrollTop();
        let header_el = $(".navbar");
        if (scroll >= 100) {
          header_el.addClass("scroll_menu");
        } else {
          header_el.removeClass("scroll_menu");
        }
      });
    },

    navigationSnippet: function () {
      $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
          // On-page links
          if (
            location.pathname.replace(/^\//, "") ==
              this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
          ) {
            // Figure out element to scroll to
            let target = $(this.hash);

            target = target.length
              ? target
              : $("[name=" + this.hash.slice(1) + "]");
            // Does a scroll target exist?
            if (target.length) {
              // Only prevent default if animation is actually gonna happen
              event.preventDefault();
              $("html, body").animate(
                {
                  scrollTop: target.offset().top,
                },
                1000,
                function () {
                  // Callback after animation
                  // Must change focus!
                  let $target = $(target);
                  $target.focus();

                  if ($(window).width() <= 768) {
                    menu_wrapper.slideUp(300);
                  }

                  if ($target.is(":focus")) {
                    // Checking if the target was focused
                    return false;
                  } else {
                    $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                    $target.focus(); // Set focus again
                  }
                }
              );
            }
          }
        });
    },

    moveSectionFooterIntoFooter: function () {
      let SectionMenu = $("#block-menu-menu-footer-menu");
      if (SectionMenu) {
        SectionMenu.insertAfter(
          ".ab-inbev-footer .ab-inbev-footer-content-1 .ab-inbev-footer-aware-logo"
        );
        $(".ab-inbev-footer-content-1 .menu-wrapper").hide();
      }
    },

    setBackgroundImage: function () {
      let Body = $("html body");
      let DataBackgroundDesktop = Body.data("bg-desktop");
      let DataBackgroundMobile = Body.data("bg-mobile");
      if (DataBackgroundMobile && isScreenMobile) {
        Body.attr(
          "style",
          'background-image:url("' + DataBackgroundMobile + '");'
        );
      } else if (DataBackgroundDesktop) {
        Body.attr(
          "style",
          'background-image:url("' + DataBackgroundDesktop + '");'
        );
      }
    },

    HideClickMenu: function () {
      let ItemMenu = $(".navbar-nav li a"),
        SectionMenu = $(".navbar-collapse"),
        BtnMenu = $(".navbar-toggle");

      ItemMenu.click(function (e) {
        if (SectionMenu.hasClass("in")) {
          SectionMenu.removeClass("in");
        }

        if (BtnMenu.hasClass("collapsed")) {
          BtnMenu.removeClass("collapsed");
          BtnMenu.attr("aria-expanded", "false");
        }
      });
    },
    sliderVideo: function () {
      let slides = $(
        ".view-slider-home .views-row .views-field-field-slider-video-youtube-id .field-content"
      );

      if (slides.length != 0) {
        slides.each(function (index) {
          var code_youtube = $.trim($(this).text());
          const YTPlayer = require("yt-player");
          const player = new YTPlayer(this, {
            controls: false,
            captions: false,
            fullscreen: true,
            width: $(window).width(),
            height: $(".field-content img").height() + 100,
          });
          player.load(code_youtube);
          player.setVolume(100);
          player.setPlaybackQuality("hd1080");
          $(window).resize(function () {
            player.setSize(
              $(window).width(),
              $(".field-content img").height() + 100
            );
          });
          //$(this).html("<iframe src='https://www.youtube.com/embed/"+code_youtube+"?controls=0&showinfo=0' ></iframe>");
        });
      }
    },
  };

  // -- Agegate -- //
  const $AgegateScope = {
    // Constructor
    init: function () {
      this.ageScripts();
      this.moveUpTextAge();
      this.centerCursorInputForEdgeBrowsers();
      this.styleForCheckboxRememberMe();
      //this.includeLabelSelectInCombo();
    },

    // scripts for Agegate
    ageScripts: function () {
      // Dom manipulation
      let ListCountries = $(".form-item-list-of-countries");
      let Checklist = $(".form-item-remember-me");
      let FbValidate = $(".age_checker_facebook_validate");
      let RememberMe = $(".ab-inbev-remember-me");
      let RememberLabel = Checklist.find("label");
      let RememberMeStr = RememberMe.find("strong");
      let FooterContent = $(".ab-inbev-footer");
      let AgeChecker = $("#age_checker");

      if (ListCountries) {
        ListCountries.insertAfter("#age_checker_error_message");
      }

      /*
            if(Checklist) {
            	Checklist.append(RememberMe);
            	RememberLabel.append(RememberMeStr);
            }*/

      $("<div class='label_or'>OR</div>").insertAfter("#edit-submit");
      if (FbValidate) {
        FbValidate.insertAfter("#age_checker_widget .label_or");
        FbValidate.append(
          '<span class="button_facebook"><div class="text-button">SIGN IN WITH FACEBOOK</div></span>'
        );
      }

      if (AgeChecker) {
        $("body.page-agegate").attr("style", AgeChecker.attr("style"));
        AgeChecker.removeAttr("style");
      }
    },

    moveUpTextAge: function () {
      let TextDisclaimer = $(".ab-inbev-remember-me");
      if (TextDisclaimer) {
        TextDisclaimer.insertAfter(".form-item-remember-me");
      }
    },

    includeLabelSelectInCombo: function () {
      let LabelSelect = $("#age_checker .form-item-list-of-countries label");
      let SelectCities = $("#age_checker_country");
      let SelectOption = $(
        '<option value="#">' + LabelSelect.text() + "</option>"
      );
      SelectCities.prepend(SelectOption);
    },

    centerCursorInputForEdgeBrowsers: function () {
      if (window.navigator.userAgent.search(/edge/gi)) {
        let InputsText = $(
          "#age_checker_day, #age_checker_month, #age_checker_year"
        );

        InputsText.each(function () {
          let PlaceholderText = $(this).attr("placeholder");
          $(this).focusin(function () {
            $(this).attr("placeholder", "");
          });

          $(this).focusout(function () {
            var txtval = $(this).val();
            if (txtval == "") {
              $(this).attr("placeholder", PlaceholderText);
            }
          });
        });
      }
    },

    styleForCheckboxRememberMe: function () {
      let LabelContainerCheckbox = $(
        "#age_checker .form-item-remember-me label"
      );
      if (LabelContainerCheckbox) {
        LabelContainerCheckbox.append('<span class="checkmark"></span>');
      }
    },
  };

  // -- Home -- //
  const $HomeScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.homeSliders();
      this.feedInstagram();
      this.feedFacebook();
    },

    // scripts for slider
    homeSliders: function () {
      let ViewContent = $(
        "#block-views-slider-home-block .view-slider-home .view-content"
      );
      if (ViewContent) {
        ViewContent.slick({
          dots: false,
          infinite: true,
          arrows: true,
        });
      }
    },

    feedInstagram: function () {
      new InstagramFeed({
        username: "nilespecialug",
        container: document.getElementById("instafeed"),
        display_profile: false,
        display_biography: false,
        display_gallery: true,
        callback: null,
        styling: true,
        items: 9,
        items_per_row: 3,
        margin: 1,
      });
    },

    feedFacebook: function () {
      var pageAccessToken =
        "EAAjXAJdBJSEBAKEr1CsJkvC1D7YszCIpFI6Bm86z1GMVcrUSeVMTlLx3ugADiEyR9lZB8IbfBQCZBUTnW1qv0xZASuE8FoAOx4omqfymKQ3ZBstQlg7uM75JY8YM3oZBOwDbZCBZCy4SwNubh3MEOAPQKrtS8Qvc9SZBQ3KXyhlr7qr7XPlkZA2fA3iqmfhpoFpFuRruaPglKigZDZD";
      $.ajaxSetup({ cache: true });
      $.getScript("https://connect.facebook.net/en_US/sdk.js", function () {
        FB.init({
          appId: "2488197351286049",
          version: "v4.0",
        });
        FB.api(
          "/167949439885337/feed",
          "GET",
          {
            access_token: pageAccessToken,
            fields: "full_picture",
            limit: 16,
          },
          function (response) {
            var feedObj = response.data;
            for (var i = 0; i < feedObj.length; i++) {
              $("#facefeed").append(
                '<div class="thumb__fb"><img src="' +
                  feedObj[i].full_picture +
                  '" /></div>'
              );
            }
          }
        );
      });
    },
  };

  // -- About -- //
  const $AboutScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.setBackgroundImageAbout();
    },
    // Fix Background Image in About
    setBackgroundImageAbout: function () {
      let Body = $("html body");
      let DataBackgroundDesktop = Body.data("bg-desktop");
      let DataBackgroundMobile = Body.data("bg-mobile");
      if (DataBackgroundMobile && isScreenMobile) {
        $(".custom-nile-about #block-system-main").attr(
          "style",
          'background-image:url("' + DataBackgroundMobile + '");'
        );
      } else if (DataBackgroundDesktop) {
        $(".custom-nile-about #block-system-main").attr(
          "style",
          'background-image:url("' + DataBackgroundDesktop + '");'
        );
      }
    },
  };

  // -- Sports -- //
  const $SportsScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.setBackgroundImageAbout();
    },
    // Fix Background Image in About
    setBackgroundImageAbout: function () {
      let Body = $("html body");
      let DataBackgroundDesktop = Body.data("bg-desktop");
      let DataBackgroundMobile = Body.data("bg-mobile");
      if (DataBackgroundMobile && isScreenMobile) {
        $(".custom-nile-sports #block-system-main").attr(
          "style",
          'background-image:url("' + DataBackgroundMobile + '");'
        );
      } else if (DataBackgroundDesktop) {
        $(".custom-nile-sports #block-system-main").attr(
          "style",
          'background-image:url("' + DataBackgroundDesktop + '");'
        );
      }
    },
  };

  // -- Contact Us -- //
  const $ContactUsScope = {
    // Constructor
    init: function () {
      // Instance functions
      this.validateForm();
      this.sendDataLayer();
    },

    // scripts for slider
    validateForm: function () {
      //Add new methods to validate fields
      jQuery.validator.addMethod(
        "emailordomain",
        function (value, element) {
          return (
            this.optional(element) ||
            /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) ||
            /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(
              value
            )
          );
        },
        "Please specify the correct url/email"
      );

      jQuery.validator.addMethod(
        "lettersonly",
        function (value, element) {
          return (
            this.optional(element) ||
            /^[A-Za-z\u00C0-\u017F" "ñ]+$/i.test(value)
          );
        },
        "Letters and spaces only please"
      );

      if ($("#webform-client-form-31").length > 0) {
        // jQuery validate
        $("#webform-client-form-31").validate({
          rules: {
            "submitted[full_name]": {
              required: true,
              lettersonly: true,
            },
            "submitted[town]": {
              required: true,
              lettersonly: true,
            },
            "submitted[message]": { required: true },
          },
          errorPlacement: function () {
            return false;
          },
        });
      }
    },

    //Send Data Layer Contact Us
    sendDataLayer: function () {
      $("#webform-client-form-31").submit(function (event) {
        if ($("#webform-client-form-31").valid()) {
          dataLayer.push({
            event: "trackEvent",
            eventCategory: "Contact Us",
            eventAction: "Click",
            eventLabel: "Submit",
            eventValue: "Submit",
          });
        }
      });
    },
  };

  // -- Contact Us -- //
  const $Register = {
    // Constructor
    init: function () {
      // Instance functions
      this.validateForm();
      this.sendDataLayer();
    },

    // scripts for slider
    validateForm: function () {
      //Add new methods to validate fields
      jQuery.validator.addMethod(
        "emailordomain",
        function (value, element) {
          return (
            this.optional(element) ||
            /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(value) ||
            /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/.test(
              value
            )
          );
        },
        "Please specify the correct url/email"
      );

      jQuery.validator.addMethod(
        "lettersonly",
        function (value, element) {
          return (
            this.optional(element) ||
            /^[A-Za-z\u00C0-\u017F" "ñ]+$/i.test(value)
          );
        },
        "Letters and spaces only please"
      );

      jQuery.validator.addMethod(
        "alphanumeric",
        function (value, element) {
          return (
            this.optional(element) ||
            /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/i.test(value)
          );
        },
        "Numbers and dashes only"
      );

      jQuery.validator.addMethod(
        "numeric",
        function (value, element) {
          return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
        },
        "Numbers and dashes only"
      );

      jQuery.validator.addMethod(
        "days",
        function (value, element) {
          return (
            this.optional(element) || /^(0[1-9]|[12]\d|3[01])*$/i.test(value)
          );
        },
        "Numbers and dashes only"
      );

      jQuery.validator.addMethod(
        "month",
        function (value, element) {
          return this.optional(element) || /^(0[1-9]|1[012])*$/i.test(value);
        },
        "Numbers and dashes only"
      );

      jQuery.validator.addMethod(
        "year",
        function (value, element) {
          return this.optional(element) || /^(\d{4})*$/i.test(value);
        },
        "Numbers and dashes only"
      );

      jQuery.validator.addMethod(
        "valueNotEquals",
        function (value, element, arg) {
          return arg !== value;
        },
        "Value must not equal arg."
      );

      if ($("#webform-client-form-91").length > 0) {
        // jQuery validate
        $("#webform-client-form-91").validate({
          rules: {
            "submitted[wrapper_left][name]": {
              required: true,
              lettersonly: true,
            },
            "submitted[wrapper_left][surname]": {
              required: true,
              lettersonly: true,
            },
            "submitted[wrapper_left][celphone]": {
              required: true,
              alphanumeric: true,
            },
            "submitted[wrapper_left][e_mail]": {
              required: true,
              emailordomain: true,
            },
            "submitted[wrapper_right][date][dd]": {
              required: true,
              days: true,
              numeric: true,
            },
            "submitted[wrapper_right][date][mm]": {
              required: true,
              month: true,
              numeric: true,
            },
            "submitted[wrapper_right][date][yyyy]": {
              required: true,
              year: true,
              numeric: true,
            },
            "submitted[wrapper_right][province]": {
              required: true,
              valueNotEquals: true,
            },
            "submitted[wrapper_right][gender]": {
              required: true,
              valueNotEquals: true,
            },
          },
          errorPlacement: function () {
            return false;
          },
        });
      }
    },

    //Send Data Layer Newsletter
    sendDataLayer: function () {
      $("#webform-client-form-91").submit(function (event) {
        if ($("#webform-client-form-91").valid()) {
          dataLayer.push({
            event: "trackEvent",
            eventCategory: "Register",
            eventAction: "Click",
            eventLabel: "Successful Registration",
            eventValue: "Submit",
          });
        }
      });
    },
  };

  // ----------------------------
  // TRIGGERS
  // ----------------------------

  // Trigger
  $GeneralScope.init();

  // Agegate
  if ($("body").hasClass("page-agegate")) {
    $AgegateScope.init();
  }

  // Home Scripts
  if ($("body").hasClass("front")) {
    $HomeScope.init();
  }

  // Contact Us Scripts
  if ($("body").hasClass("custom-contact-us")) {
    $ContactUsScope.init();
  }
  // Register
  if ($("body").hasClass("custom-register-newsletter")) {
    $Register.init();
  }
  // About Scripts
  if ($("body").hasClass("custom-nile-about")) {
    $AboutScope.init();
  }

  // Sports Scripts
  if ($("body").hasClass("custom-nile-sports")) {
    $SportsScope.init();
  }
});
